# PelicanBackEnd
Java back-end  for Pelican Proj

Used technology:

- spring-web
- CrudRepository
- liquibase
- spring jpa


cat pelican.txt | psql -U rest rest -p 15432 -h localhost


build project: cd docker && make build-all push
