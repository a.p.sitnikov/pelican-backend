package ru.usque.pelican.common;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Slf4j
class Common {
    @Test
    void test() {
//    19.01.2019
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.YYYY");
        String line = format.format(new Date());
        log.info("line: {}", line);
    }

    @Test
    void test3() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDateTime dateTime = LocalDateTime.now();
        final String format1 = dateTime.format(formatter);
        System.out.println(format1);
    }

    @SneakyThrows
    @Test
    void test4() {
        String date = "11.12.2020";
        val dateTime = LocalDate.parse(date,DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        System.out.println(dateTime);
    }
}
