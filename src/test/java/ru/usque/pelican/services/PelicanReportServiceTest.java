package ru.usque.pelican.services;

import org.junit.jupiter.api.Test;
import ru.usque.pelican.dto.PelicanLevelReport;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PelicanReportServiceTest {

    @Test
    void calcLevelReport1() {
        final PelicanReportService v = new PelicanReportService(null, null, null);
        final PelicanLevelReport a = v.calculatePelicanLevelReportImpl("a", 260);
        assertEquals(2,a.getLevel().intValue());
        assertEquals(60,a.getCurrentPercent().doubleValue());
    }

    @Test
    void calcLevelReport2() {
        final PelicanReportService v = new PelicanReportService(null, null, null);
        final PelicanLevelReport a = v.calculatePelicanLevelReportImpl("a", 100);
        assertEquals(1,a.getLevel().intValue());
        assertEquals(0,a.getCurrentPercent().doubleValue());
    }

    @Test
    void calcLevelReport3() {
        final PelicanReportService v = new PelicanReportService(null, null, null);
        final PelicanLevelReport a = v.calculatePelicanLevelReportImpl("a", 20);
        assertEquals(0,a.getLevel().intValue());
        assertEquals(20,a.getCurrentPercent().doubleValue());
    }

    @Test
    void calcLevelReport4() {
        final PelicanReportService v = new PelicanReportService(null, null, null);
        final PelicanLevelReport a = v.calculatePelicanLevelReportImpl("a", 120);
        assertEquals(1,a.getLevel().intValue());
        assertEquals(20,a.getCurrentPercent().doubleValue());
    }

    @Test
    void dateTimeParse(){
        String dateTime = "16.01.2020 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        LocalDateTime formatDateTime = LocalDateTime.parse(dateTime, formatter);
        System.out.println("Parsed Date : " + formatDateTime);
    }


}
