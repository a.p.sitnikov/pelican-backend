package ru.usque.pelican.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.usque.pelican.entities.PelicanPlan;
import ru.usque.pelican.repository.PelicanPlansRepository;
import ru.usque.pelican.repository.PelicanUserRepository;
import ru.usque.pelican.services.interfaces.IPelicanPlansService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.usque.pelican.controller.util.ControllerUtils.generateNowDate;

@Service
public class PelicanPlanService implements IPelicanPlansService {
    private final PelicanPlansRepository repository;
    private final PelicanUserRepository userRepository;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public PelicanPlanService(PelicanPlansRepository repository, PelicanUserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Override
    public List<PelicanPlan> findAll() {
        List<PelicanPlan> scores = new ArrayList<>();
        repository.findAll().forEach(scores::add);
        return scores;
    }

    @Override
    public List<PelicanPlan> findByUserId(Integer userId, Boolean isGrand) {
        List<PelicanPlan> userPlans = repository.findByUserId(userId)
                .stream()
                .filter(e-> e.getIsGrand() || !e.getIsFinished())
                .sorted(Comparator.comparing(PelicanPlan::getOrderNumber))
                .collect(Collectors.toList());
        if (isGrand == null) {
            if (userPlans.isEmpty()) {
                PelicanPlan plan = createDefault(userId);
                userPlans.add(plan);
            }
            return userPlans;
        }
        List<PelicanPlan> planByUser = userPlans.stream().filter(e -> e.getIsGrand().equals(isGrand)).collect(Collectors.toList());
        if (planByUser.isEmpty() && !isGrand) {
            PelicanPlan plan = createDefault(userId);
            planByUser.add(plan);
        }

        return planByUser;
    }

    private PelicanPlan createDefault(Integer userId) {
        PelicanPlan plan = new PelicanPlan();
        plan.setDate(generateNowDate());
        plan.setIsFinished(false);
        plan.setOrderNumber(1);
        plan.setName("This is default task.");
        plan.setUser(userRepository.findById(userId));
        plan.setIsGrand(false);
        plan.setScore(500);
        addPlan(plan);
        return plan;
    }


    @Override
    public PelicanPlan addPlan(PelicanPlan plan) {
        return repository.save(plan);
    }

    @Override
    public PelicanPlan updatePlan(PelicanPlan plan) {
        return repository.save(plan);
    }

    @Override
    public void deletePlan(Integer id) {
        repository.delete(id);
    }

    @Override
    public PelicanPlan findById(Integer id) {
        return repository.findById(id);
    }


}
