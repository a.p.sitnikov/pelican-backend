package ru.usque.pelican.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.usque.pelican.dto.PelicanLevelReport;
import ru.usque.pelican.dto.PelicanReport;
import ru.usque.pelican.dto.PelicanYearReport;
import ru.usque.pelican.entities.*;
import ru.usque.pelican.services.interfaces.IPelicanBadEventService;
import ru.usque.pelican.services.interfaces.IPelicanEventService;
import ru.usque.pelican.services.interfaces.IPelicanPlansService;
import ru.usque.pelican.services.interfaces.IPelicanReportService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
public class PelicanReportService implements IPelicanReportService {

    private final IPelicanEventService eventService;
    private final IPelicanBadEventService badEventService;
    private final IPelicanPlansService pelicanPlanService;

    @Autowired
    public PelicanReportService(IPelicanEventService eventService,
                                IPelicanBadEventService badEventService,
                                IPelicanPlansService pelicanPlanService) {
        this.eventService = eventService;
        this.badEventService = badEventService;
        this.pelicanPlanService = pelicanPlanService;
    }

    @Override
    public List<PelicanReport> calcCommonReport(int month, PelicanUser user) {
        List<PelicanReport> reports = new ArrayList<>();
        List<PelicanEvent> events = new ArrayList<>();
        int days = 0;
        for (int i = 0; i < month; i++) {
            LocalDateTime dateTime = LocalDateTime.now().minusMonths(i);
            days += dateTime.toLocalDate().lengthOfMonth();
            events.addAll(eventService.findByUserWithDate(user.getId(), dateTime));
        }

//        final double sum = events.stream().mapToDouble(PelicanEvent::calculateScore).sum();
        final double sum = events.stream()
                .collect(Collectors.groupingBy(PelicanEvent::getDateTime, Collectors.toList()))
                .values().stream().mapToDouble(pelicanEvents -> {
                    final double v = pelicanEvents.stream().mapToDouble(PelicanEvent::calculateScore).sum();
                    if (v > 100) {
                        return 100d;
                    }
                    return v;
                }).sum();

        final double done = sum / days;
        reports.add(new PelicanReport("Done", Double.valueOf(done).intValue()));
        reports.add(new PelicanReport("Remain", Double.valueOf(100d - done).intValue()));
        return reports;
    }

    @Override
    public List<PelicanReport> calcCategoryReport(int month, PelicanUser user, boolean isCalcCategory) {
        List<PelicanEvent> events = new ArrayList<>();

        for (int i = 0; i < month; i++) {
            LocalDateTime dateTime = LocalDateTime.now().minusMonths(i);
            events.addAll(eventService.findByUserWithDate(user.getId(), dateTime));
        }

        return events.stream()
                .collect(Collectors.groupingBy(o -> o.getCategory().getParent().getName(), Collectors.toList()))
                .entrySet()
                .stream()
                .map(entry -> new PelicanReport(entry.getKey(), entry.getValue(),isCalcCategory))
                .collect(Collectors.toList());
    }

    @Override
    public List<PelicanReport> calcAllCategoryReport(int month, PelicanUser user) {
        List<PelicanEvent> events = new ArrayList<>();

        for (int i = 0; i < month; i++) {
            LocalDateTime dateTime = LocalDateTime.now().minusMonths(i);
            events.addAll(eventService.findByUserWithDate(user.getId(), dateTime));
        }


        return events.stream().collect(Collectors.groupingBy(o -> {
            final PelicanCategory category = o.getCategory();
            final String prefix = category.getParent() != null ? category.getParent().getName() + ": " : "";
            return prefix + category.getName();
        }, Collectors.summingDouble(value -> {
            final Integer score = value.getScore();
            if (score == null && value.getCategory().getSimple()) {
                return value.getCategory().getScore();
            }

            if (score == null) {
                return 0;
            }
            return score;
        })))
                .entrySet()
                .stream()
                .map(entry -> new PelicanReport(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public List<PelicanReport> calcEntertainmentCommonReport(int month, PelicanUser user) {
        List<PelicanReport> reports = new ArrayList<>();
        List<PelicanBadEvent> events = new ArrayList<>();
        int days = 0;
        for (int i = 0; i < month; i++) {
            LocalDateTime dateTime = LocalDateTime.now().minusMonths(i);
            days += dateTime.toLocalDate().lengthOfMonth();
            events.addAll(badEventService.findByUserWithDate(user.getId(), dateTime));
        }

        final double sum = events.stream().mapToDouble(PelicanBadEvent::getScore).sum();
        final double done = sum / days;
        reports.add(new PelicanReport("Done", Double.valueOf(done).intValue()));
        reports.add(new PelicanReport("Remain", Double.valueOf(100d - done).intValue()));
        return reports;
    }

    @Override
    public List<PelicanReport> calcEntertainmentCategoryReport(int month, PelicanUser user) {
        List<PelicanBadEvent> events = new ArrayList<>();

        for (int i = 0; i < month; i++) {
            LocalDateTime dateTime = LocalDateTime.now().minusMonths(i);
            events.addAll(badEventService.findByUserWithDate(user.getId(), dateTime));
        }

        return events.stream().collect(Collectors.groupingBy(o -> o.getCategory().getName(), Collectors.summingDouble(PelicanBadEvent::getScore)))
                .entrySet()
                .stream()
                .map(entry -> new PelicanReport(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public List<PelicanLevelReport> calcLevelReport(PelicanUser user) {
        return eventService.findByUserId(user.getId())
                .stream()
                .collect(
                        Collectors.groupingBy(pelicanEvent -> pelicanEvent.getCategory().getParent().getName(),
                                Collectors.toList())
                )
                .entrySet()
                .stream()
                .map(entry -> {
                    final double totalScorePerCategory = entry.getValue().stream().mapToDouble(PelicanEvent::calculateScore).sum();
                    return calculatePelicanLevelReportImpl(entry.getKey(), totalScorePerCategory);
                })
                .sorted(Comparator.comparing(PelicanLevelReport::getLevel).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public List<PelicanYearReport> calcAnnualReport(PelicanUser user) {
        Map<Integer, List<PelicanPlan>> greatPlans = pelicanPlanService.findByUserId(user.getId(), true)
                .stream()
                .filter(PelicanPlan::getIsFinished)
                .collect(groupingBy(e -> e.getDateTime().getYear()));


        return eventService.findByUserId(user.getId()).stream().collect(groupingBy(e -> e.getDateTime().getYear(), Collectors.toList()))
                .entrySet()
                .stream()
                .map(year2events -> {
                    PelicanYearReport year = new PelicanYearReport();
                    year.setYear(year2events.getKey());
                    List<PelicanReport> categoriesReport = year2events.getValue()
                            .stream()
                            .collect(groupingBy(e -> e.getCategory().getParent(), Collectors.toList()))
                            .entrySet()
                            .stream()
                            .map(parentCat2events -> {
                                PelicanReport report = new PelicanReport();
                                report.setName(parentCat2events.getKey().getName());
                                report.setValue((int) parentCat2events.getValue().stream().mapToDouble(PelicanEvent::calculateScore).sum());

                                List<PelicanReport> subs = year2events.getValue()
                                        .stream()
                                        .filter(e -> e.getCategory().getParent().equals(parentCat2events.getKey()))
                                        .collect(groupingBy(PelicanEvent::getCategory))
                                        .entrySet()
                                        .stream()
                                        .map(e -> {
                                            PelicanReport sub = new PelicanReport();
                                            sub.setName(e.getKey().getName());
                                            sub.setValue((int) e.getValue().stream().mapToDouble(PelicanEvent::calculateScore).sum());
                                            return sub;
                                        }).collect(Collectors.toList());

                                report.setSubReport(subs);
                                return report;
                            }).collect(Collectors.toList());

                    year.setCategoriesReports(categoriesReport);
                    final List<PelicanPlan> pelicanPlans = greatPlans.get(year2events.getKey());
                    if (pelicanPlans != null) {
                        pelicanPlans.sort(Comparator.comparing(PelicanPlan::getDateTime));
                    }
                    year.setGreatPlans(pelicanPlans);
                    return year;
                }).collect(Collectors.toList());
    }

    PelicanLevelReport calculatePelicanLevelReportImpl(String name, double totalScore) {
        int level = 0;
        int levelScore = 0;
        double percent = 0;

        while (levelScore < totalScore) {


            final double delta = totalScore - levelScore;
            if (delta < 100) {
                percent = delta;
                break;
            }

            levelScore += 100;
            level++;

        }
        return new PelicanLevelReport(name, level, percent);
    }
}
