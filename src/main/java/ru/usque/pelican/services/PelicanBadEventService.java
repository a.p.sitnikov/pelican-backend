package ru.usque.pelican.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.usque.pelican.entities.PelicanBadEvent;
import ru.usque.pelican.entities.PelicanEvent;
import ru.usque.pelican.repository.PelicanBadEventRepository;
import ru.usque.pelican.services.interfaces.IPelicanBadEventService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PelicanBadEventService implements IPelicanBadEventService {
    private final PelicanBadEventRepository repository;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public PelicanBadEventService(PelicanBadEventRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<PelicanBadEvent> findAll() {
        List<PelicanBadEvent> events = new ArrayList<>();
        repository.findAll().forEach(events::add);
        return events;
    }

    @Override
    public List<PelicanBadEvent> findByUserId(Integer id) {
        return repository.findByUserId(id);
    }

    @Override
    public List<PelicanBadEvent> findByUserWithDate(Integer userId, LocalDateTime dateTime) {
        final LocalDate localDate = dateTime.toLocalDate();
        LocalDateTime start = localDate.withDayOfMonth(1).atStartOfDay();
        LocalDateTime end = localDate.withDayOfMonth(localDate.lengthOfMonth()).atStartOfDay().plusDays(1);

        final List<PelicanBadEvent> events = em.createQuery("select o from PelicanBadEvent o where o.user.id = :userId " +
                    "and o.dateTime >= :startDate and o.dateTime <= :endDate", PelicanBadEvent.class)
                .setParameter("userId", userId)
                .setParameter("startDate", start)
                .setParameter("endDate", end)
                .getResultList();
        return events;
    }

    @Override
    public PelicanBadEvent findById(Integer id) {
        return repository.findById(id);
    }

    @Override
    public PelicanBadEvent addBadEvent(PelicanBadEvent event) {
        return repository.save(event);
    }

    @Override
    public PelicanBadEvent updateBadEvent(PelicanBadEvent event) {
        return repository.save(event);
    }

    @Override
    public void deleteBadEvent(Integer id) {
        repository.delete(id);
    }


}
