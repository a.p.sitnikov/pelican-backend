package ru.usque.pelican.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.usque.pelican.entities.PelicanEvent;
import ru.usque.pelican.repository.PelicanEventRepository;
import ru.usque.pelican.services.interfaces.IPelicanEventService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PelicanEventService implements IPelicanEventService {
    private final PelicanEventRepository repository;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public PelicanEventService(PelicanEventRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<PelicanEvent> findAll() {
        List<PelicanEvent> events = new ArrayList<>();
        repository.findAll().forEach(events::add);
        return events;
    }

    @Override
    public List<PelicanEvent> findByUserId(Integer id) {
        return repository.findByUserId(id);
    }

    @Override
    public List<PelicanEvent> findByUserWithDate(Integer userId, LocalDateTime dateTime) {

        final LocalDate localDate = dateTime.toLocalDate();
        LocalDateTime start = localDate.withDayOfMonth(1).atStartOfDay();
        LocalDateTime end = localDate.withDayOfMonth(localDate.lengthOfMonth()).atStartOfDay().plusDays(1);

        final List<PelicanEvent> events = em.createQuery("select o from PelicanEvent o where o.user.id = :userId " +
                "and o.dateTime >= :startDate and o.dateTime <= :endDate", PelicanEvent.class)
                .setParameter("userId", userId)
                .setParameter("startDate", start)
                .setParameter("endDate", end)
                .getResultList();
        return events;
    }

    @Override
    public PelicanEvent findById(Integer id) {
        return repository.findById(id);
    }

    @Override
    public boolean addEvent(PelicanEvent event) {
        repository.save(event);
        return true;
    }

    @Override
    public PelicanEvent updateEvent(PelicanEvent event) {
        return repository.save(event);
    }

    @Override
    public void deleteEvent(Integer id) {
        repository.delete(id);
    }


}
