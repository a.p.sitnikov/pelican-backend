package ru.usque.pelican.services.interfaces;

import ru.usque.pelican.entities.PelicanEvent;

import java.time.LocalDateTime;
import java.util.List;

public interface IPelicanEventService {
    List<PelicanEvent> findAll();
    List<PelicanEvent> findByUserId(Integer id);
    List<PelicanEvent> findByUserWithDate(Integer userId, LocalDateTime dateTime);
    PelicanEvent findById(Integer id);
    boolean addEvent(PelicanEvent event);
    PelicanEvent updateEvent(PelicanEvent event);
    void deleteEvent(Integer event);
}
