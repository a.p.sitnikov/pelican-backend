package ru.usque.pelican.services.interfaces;

import ru.usque.pelican.dto.PelicanLevelReport;
import ru.usque.pelican.dto.PelicanReport;
import ru.usque.pelican.dto.PelicanYearReport;
import ru.usque.pelican.entities.PelicanUser;

import java.util.List;

public interface IPelicanReportService {
    List<PelicanReport> calcCommonReport(int month, PelicanUser user);

    List<PelicanReport> calcCategoryReport(int month, PelicanUser user, boolean isCalcCategory);

    List<PelicanReport> calcAllCategoryReport(int month, PelicanUser user);

    List<PelicanReport> calcEntertainmentCommonReport(int month, PelicanUser user);

    List<PelicanReport> calcEntertainmentCategoryReport(int days, PelicanUser user);

    List<PelicanLevelReport> calcLevelReport(PelicanUser user);

    List<PelicanYearReport> calcAnnualReport(PelicanUser user);
}
