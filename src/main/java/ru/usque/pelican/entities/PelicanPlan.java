package ru.usque.pelican.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static ru.usque.pelican.entities.PelicanEvent.FORMAT;
import static ru.usque.pelican.entities.PelicanEvent.FORMAT_DATE_TIME;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "pelican_plan")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NONE)
public class PelicanPlan implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private PelicanUser user;

    @Column(name = "name")
    private String name;

    @JsonIgnore
    @Column(name = "date")
    private LocalDateTime dateTime;

    @Transient
    private String date;

    @Column(name = "is_grand")
    private Boolean isGrand;

    @Column(name = "score")
    private Integer score;

    @Column(name = "is_finished")
    private Boolean isFinished;

    @Column(name = "order_nuber")
    private Integer orderNumber;

    public Boolean getIsFinished() {
        if (isFinished != null) {
            return isFinished;
        }
        return false;
    }

    public Boolean getIsGrand() {
        if (isGrand == null) {
            return false;
        }
        return isGrand;
    }

    public String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        if (dateTime != null) {
            return dateTime.format(formatter);
        }
        return LocalDate.now().format(formatter);
    }

    public LocalDateTime restoreDate(){
        if (date != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_TIME);
            return LocalDateTime.parse(date + " 00:00", formatter);
        }
        return LocalDateTime.now();
    }
}
