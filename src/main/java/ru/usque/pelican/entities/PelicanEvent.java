package ru.usque.pelican.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "pelican_event")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NONE)
public class PelicanEvent implements Serializable {
    public static final String FORMAT = "dd.MM.yyyy";
    public static final String FORMAT_DATE_TIME = "dd.MM.yyyy HH:mm";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private PelicanUser user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private PelicanCategory category;

    @Column(name = "score")
    private Integer score;

    @JsonIgnore
    @Column(name = "date")
    private LocalDateTime dateTime;

    @Transient
    private String date;

    @Transient
    private double balls;

    public PelicanEvent(PelicanEvent event) {
        this.id = event.getId();
        this.user = event.getUser();
        this.category = event.getCategory();
        this.score = event.getScore();
        this.dateTime = event.getDateTime();
        this.date = event.getDate();
        this.balls = event.getBalls();

        if (date != null && dateTime == null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
            dateTime = LocalDate.parse(event.date, formatter).atStartOfDay();
        }
    }

    public String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        if (dateTime != null) {
            return dateTime.format(formatter);
        }
        return LocalDate.now().format(formatter);
    }

    public double getBalls() {
        return calculateScore();
    }

    //получить очки события.
    public double calculateScore() {
        if (category.getSimple()) {
            return category.getScore();
        }

        if (category.getScore() <= 0) {
            return 0;
        }

        if (category.getScore() == null || score == null) {
            return 0d;
        }

        double value = score * 100.0 / category.getScore();
        if (value > 100) {
            value = 100d;
        }
        return value;
    }
}
