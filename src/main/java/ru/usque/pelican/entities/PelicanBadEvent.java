package ru.usque.pelican.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static ru.usque.pelican.entities.PelicanEvent.FORMAT;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "pelican_bad_event")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NONE)
public class PelicanBadEvent implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private PelicanUser user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bad_category_id")
    private PelicanBadCategory category;

    @JsonIgnore
    @Column(name = "date")
    private LocalDateTime dateTime;

    @Transient
    private String date;

    @Transient
    private double balls;

    //получить очки события.
    public double getScore() {

        if (category.getScore() <= 0) {
            return 0;
        }

        return category.getScore();
    }

    public PelicanBadEvent(PelicanBadEvent event) {
        this.id = event.getId();
        this.user = event.getUser();
        this.category = event.getCategory();
        this.dateTime = event.getDateTime();
        this.date = event.getDate();
        this.balls = event.getBalls();

        if (date != null && dateTime == null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
            dateTime = LocalDate.parse(event.date, formatter).atStartOfDay();
        }
    }

    public double getBalls() {
        return getScore();
    }


    public String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        if (dateTime != null) {
            return dateTime.format(formatter);
        }
        return LocalDate.now().format(formatter);
    }
}
