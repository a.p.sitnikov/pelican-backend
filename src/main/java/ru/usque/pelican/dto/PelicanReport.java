package ru.usque.pelican.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.usque.pelican.entities.PelicanEvent;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PelicanReport {
    private String name;
    private Integer value;
    private List<PelicanReport> subReport;

    public PelicanReport(String name, double value) {
        this.name = name;
        this.value = Double.valueOf(value).intValue();
    }

    public PelicanReport(String name, List<PelicanEvent> value, boolean isCalcScore) {
        final double sum = value.stream().mapToDouble(PelicanEvent::calculateScore).sum();
        this.value = Double.valueOf(sum).intValue();
        this.name = name;

        final Collector<PelicanEvent, ?, Double> isScoreCalc = isCalcScore ? Collectors.summingDouble(PelicanEvent::calculateScore) : Collectors.summingDouble(v->{
            if (v.getScore() == null) {
                return 1;
            }
            return v.getScore();
        });
        subReport = value.stream().collect(Collectors.groupingBy(PelicanEvent::getCategory, isScoreCalc))
                .entrySet()
                .stream()
                .map(entry -> new PelicanReport(entry.getKey().getName(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
