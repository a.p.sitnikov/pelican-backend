package ru.usque.pelican.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.usque.pelican.entities.PelicanPlan;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PelicanYearReport {
    private Integer year;
    private List<PelicanReport> categoriesReports;
    private List<PelicanPlan> greatPlans;
}
