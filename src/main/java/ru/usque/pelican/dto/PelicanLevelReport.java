package ru.usque.pelican.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PelicanLevelReport {
    private String categoryName;
    private Integer level;
    private Double currentPercent;
}
