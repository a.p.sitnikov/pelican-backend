package ru.usque.pelican.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.usque.pelican.controller.util.ControllerUtils;
import ru.usque.pelican.dto.PelicanLevelReport;
import ru.usque.pelican.dto.PelicanReport;
import ru.usque.pelican.dto.PelicanUsersReport;
import ru.usque.pelican.dto.PelicanYearReport;
import ru.usque.pelican.entities.PelicanUser;
import ru.usque.pelican.services.interfaces.IPelicanReportService;
import ru.usque.pelican.services.interfaces.IPelicanUserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("report")
public class PelicanReportController {

    private final IPelicanReportService reportService;
    private final IPelicanUserService userService;

    @Autowired
    public PelicanReportController(IPelicanReportService reportService, IPelicanUserService userService) {
        this.reportService = reportService;
        this.userService = userService;
    }

    @GetMapping("common")
    public ResponseEntity<List<PelicanReport>> getAvgReport(HttpServletRequest request,
                                                            @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getAvgReport: {} {}", month, user);
        return new ResponseEntity<>(reportService.calcCommonReport(month, user), HttpStatus.OK);
    }

    @GetMapping("common/category")
    public ResponseEntity<List<PelicanReport>> getCategoryReport(HttpServletRequest request,
                                                                 @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getCategoryReport: {} {}", month, user);
        return new ResponseEntity<>(reportService.calcCategoryReport(month, user,true), HttpStatus.OK);
    }

    @GetMapping("common/raw-category")
    public ResponseEntity<List<PelicanReport>> getCategoryRawReport(HttpServletRequest request,
                                                                 @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getCategoryReport: {} {}", month, user);
        return new ResponseEntity<>(reportService.calcCategoryReport(month, user, false), HttpStatus.OK);
    }

    @GetMapping("common/all-category")
    public ResponseEntity<List<PelicanReport>> getAllCategoryReport(HttpServletRequest request,
                                                                 @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getCategoryReport: {} {}", month, user);
        return new ResponseEntity<>(reportService.calcAllCategoryReport(month, user), HttpStatus.OK);
    }

    @GetMapping("common/all-users-category")
    public ResponseEntity<List<PelicanUsersReport>> getAllUsersCategoryReport(HttpServletRequest request,
                                                                    @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getCategoryReport: {} {}", month, user);

        final List<PelicanUsersReport> reports = new ArrayList<>();
        //noinspection ConstantConditions
        final Integer currentUserId = user.getId();
        userService.findAll().forEach(e-> {
            if (!e.getId().equals(currentUserId)) {
                final PelicanUsersReport e1 = new PelicanUsersReport(e.getName(), reportService.calcAllCategoryReport(month, e));
                if (!e1.getReports().isEmpty()) {
                    reports.add(e1);
                }
            }
        });

        return new ResponseEntity<>(reports, HttpStatus.OK);
    }

    @GetMapping("entertainment")
    public ResponseEntity<List<PelicanReport>> getEntertainmentReport(HttpServletRequest request,
                                                                      @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getEntertainmentReport: {} {}", month, user);
        return new ResponseEntity<>(reportService.calcEntertainmentCommonReport(month, user), HttpStatus.OK);
    }

    @GetMapping("entertainment/category")
    public ResponseEntity<List<PelicanReport>> getEntertainmentCategoryReport(HttpServletRequest request,
                                                                              @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getEntertainmentReport: {} {}", month, user);

        return new ResponseEntity<>(reportService.calcEntertainmentCategoryReport(month, user), HttpStatus.OK);
    }

    @GetMapping("annual")
    public ResponseEntity<List<PelicanYearReport>> annualReport(HttpServletRequest request,
                                                                @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("annualReport: {} {}", month, user);

        return new ResponseEntity<>(reportService.calcAnnualReport(user), HttpStatus.OK);
    }

    @GetMapping("levels")
    public ResponseEntity<List<PelicanLevelReport>> getCategoryLevels(HttpServletRequest request,
                                                                      @RequestParam(name = "month",defaultValue = "1") int month) {
        final PelicanUser user = ControllerUtils.getUserFromRequest(request);
        log.info("getEntertainmentReport: {} {}", month, user);
        return new ResponseEntity<>(reportService.calcLevelReport(user), HttpStatus.OK);
    }

    @RequestMapping(
            value = "/**",
            method = RequestMethod.OPTIONS
    )
    public ResponseEntity handle() {
        return new ResponseEntity(HttpStatus.OK);
    }
}
