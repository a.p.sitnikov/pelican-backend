package ru.usque.pelican.controller.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import ru.usque.pelican.config.PelicanUserPrincipal;
import ru.usque.pelican.entities.PelicanUser;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Supplier;

public class ControllerUtils {

    private static boolean hasAccess(HttpServletRequest request, Integer originalUserId) {
        final PelicanUser user = getUserFromRequest(request);
        return user != null && user.getId().equals(originalUserId);
    }

    public static PelicanUser getUserFromRequest(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        if (principal == null) {
            return null;
        }
        if (principal.getClass().isAssignableFrom(PelicanUserPrincipal.class)) {
            return ((PelicanUserPrincipal) principal).getUser();
        } else if (principal.getClass().isAssignableFrom(UsernamePasswordAuthenticationToken.class)) {
            return ((PelicanUserPrincipal) ((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getPrincipal()).getUser();
        }
        return null;
    }

    public static <T> ResponseEntity<T> callResponse(HttpServletRequest request, Integer userId, Supplier<T> supplier) {
        if (hasAccess(request, userId)) {
            T t = supplier.get();
            if (t == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(t, HttpStatus.CREATED);
        }else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    public static String generateNowDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.YYYY");
        return format.format(new Date());

    }

}
